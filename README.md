# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [My shopping website]
* Key functions (add/delete)
    Going through this shopping website, 6 html pages are used to built a fuctional shopping site.
    They are index.html, men(women)products.html, product-details.html, cart.html, payment.html, db.html    respectively being used to...
                網站首頁        商品分類總覽            商品細節            購物區       付款登記     用戶資料
                
    index.html 網站首頁 
       1. [function init()] : 登入認證的主要func,也會應用在之後其他頁面
       2. 此頁面的css(ex: container, main-header等)和其他頁面大致相同，為了維持一貫性和美觀
       3. hover.js 把hover功能在手機端無法偵測到的問題解決

    men(women)products.html  商品分類總覽 
       1. js function並無多加設置，僅css部分改變

    product-details.html 商品細節 
       1. js function並無多加設置，僅css部分改變

    cart.html 購物區 
       1. 此頁面的加總購買金額之js為模板 
       2. btntopay.addEventListener 紀錄加總之金額到firebase

    payment.html 付款登記 
       1. get_card_details.addEventListener紀錄用戶付款資料到firebase

    db.html 用戶資料  
    1. write.addEventListener紀錄用戶資料到firebase 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|  
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
    此website的css file大部分是自己打的，除了商品頁面(product-details.html)中瀏覽商品細節及切換購買細節的部份是使用模板。
    在登入方式的部分有email sign up,還有第3方認證的google和fb。
    Database的部分搭配sign up , sign in method存取登入者的email和密碼，在使用者資料頁面(db.html)中給予使用者更新自己資料的頁面，此database一旦寫入會覆蓋上次的資料，並顯示在網頁上。
    在購物區(cart.html )的總金額和購買項目名稱及金額會存取到db,付款頁面(payment.html)的資料會存取到db，並顯示在頁面。
    在商品瀏覽的部分運用css.hover和animation功能讓點選商品時更有趣。
    RWD的部分在iphone6和i6 plus和i6s 和i6s plus有調整。

## Security Report (Optional) 
    在這個網站中，唯有登入者能使用的功能是付款和更新個人資料，所以在每個頁面的function init()中，認證使用者登入時，才會顯示有更新資料頁面的連結(My Profile)和購買商品的連結(My Cart)。
    並且在使用者完成付款資訊提交後，會讓使用者返回主畫面，防止信用卡資料外洩。
    在使用者更新資料頁面的連結(My Profile)會顯示使用者即時更新的資料，這裡的取db方式是用uid為節點。因為建db時就是以uid為節點向下延伸其他資料，所以顯示當前使用者資料時只要比對當前uid和db儲存的uid,
    就可以在頁面上只顯示當前使用者的資料。
