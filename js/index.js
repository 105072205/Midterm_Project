function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" + "</span><a class='dropdown-item' href='cart.html'>My Cart</a></span>"+ "</span><a class='dropdown-item' href='db.html'>My Profile</a></span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                window.location.assign("index.html");
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
                    
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
/*
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
    });                             

    var str_before_username = "<div id ='post_list'><h7 class='pointul'>Your profile updates</h7>";

    var str_after_content = "</div>\n";


    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));  */
}

window.onload = function () {
    init();
}