/* Card.js plugin by Jesse Pollak. https://github.com/jessepollak/card */
(function(){
    $('form').card({
        container: '.card-wrapper',
        width: 280,
    
        formSelectors: {
            nameInput: 'input[name="first-name"], input[name="last-name"]'
        }
    });
     
    const get_card_details = document.getElementById('input-button');
    const first_name = document.getElementById('column-left');
    const last_name = document.getElementById('column-right');
    const number = document.getElementById('input-field');
    const expiry = document.getElementById('column-left2');
    const cvc = document.getElementById('column-right2');
    const streetaddress = document.getElementById('input-field2');
    const city = document.getElementById('column-left3');
    const zipcode = document.getElementById('column-right3');
    const email = document.getElementById('input-field3');
     
/*
    get_card_details.addEventListener('click', function () {
                var newpostref = firebase.database().ref('Card_Details').push();
                newpostref.set({
                    First_name : first_name.value,
                    Last_name : last_name.value,
                    Number : number.value,
                    Expiry: expiry.value,
                    Cvc : cvc.value,
                    Streetaddress: streetaddress.value,
                    City: city.value,
                    Zipcode: zipcode.value,
                    Email: email.value
                });
            alert("Submit Successful, Now go back to homepage!");
            window.location.assign("index.html");
        });

  */  
        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
        var str_btw =  "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
        var str_after_content = "</p></div></div>\n";

        firebase.auth().onAuthStateChanged(function (user) {
            var menu = document.getElementById('dynamic-menu');
            if (user) {
                user_email = user.email;
                menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" + "</span><a class='dropdown-item' href='cart.html'>My Cart</a></span>"+ "</span><a class='dropdown-item' href='db.html'>My Profile</a></span>";
                var logout_button = document.getElementById('logout-btn');
                logout_button.addEventListener('click', function () {
                    window.location.assign("index.html");
                    firebase.auth().signOut()
                        .then(function () {
                            alert('Sign Out!')
                        })
                        .catch(function (error) {
                            alert('Sign Out Error!')
                        });
                });

                get_card_details.addEventListener('click', e=>{

                    loginUser = firebase.auth().currentUser;
                
                    const id = (new Date).getTime();
         
        
                    firebase.database().ref('users/' + user.uid ).push().set({
                        email: user.email,
                        First_name : first_name.value,
                        Last_name : last_name.value,
                        Number : number.value,
                        Expiry: expiry.value,
                        Cvc : cvc.value,
                        Streetaddress: streetaddress.value,
                        City: city.value,
                        Zipcode: zipcode.value,
                        Email: email.value
                    })
                    //alert(user.uid);
                    //window.location.assign("product-details.html");
                })
                
            } else {
                menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>"; 
            }
        });

       

    /*
            var postsRef = firebase.database().ref('pay_details/' + loginUser.uid );
            var total_post = [];
            var first_count = 0;
            var second_count = 0;  
        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (snapshot) {
                    alert("your oldest data");
                    var childData =snapshot.val();
                    total_post[total_post.length]  =  str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Streetaddress:<br>" + childData.Streetaddress + "</p>" + str_btw + "City:<br>" + childData.City +  "</p>" + str_btw + "Zipcode:<br>" + childData.Zipcode + "</p>" + str_btw + "First_name:<br>" + childData.First_name + str_after_content;
                    first_count += 1;
                });
                document.getElementById('post_list2').innerHTML = total_post.join('');

                //add listener
                postsRef.on('pay_details/' + loginUser.uid  + '/child_added', function (data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData =data.val();
                        total_post[total_post.length]  = str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Streetaddress:<br>" + childData.Streetaddress + "</p>" + str_btw + "City:<br>" + childData.City +  "</p>" + str_btw + "Zipcode:<br>" + childData.Zipcode + "</p>" + str_btw + "First_name:<br>" + childData.First_name + str_after_content;
                        document.getElementById('post_list2').innerHTML = total_post.join('');
                    }
                });
            })
            
    */
    
   /* 
        get_card_details.addEventListener('click', e=>{
    
        firebase.database().ref('users/' + loginUser.uid + '/Payment').once('value').then(function(snapshot) {
            var childData =snapshot.val();
            document.getElementById('post_list2').innerHTML = str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Streetaddress:<br>" + childData.Streetaddress + "</p>" + str_btw + "City:<br>" + childData.City +  "</p>" + str_btw + "Zipcode:<br>" + childData.Zipcode + "</p>" + str_btw + "First_name:<br>" + childData.First_name + str_after_content;
        });
    })

/*
    var postsRef = firebase.database().ref('users/' + loginUser.uid + '/Payment');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    alert("loginUFFFser.uid");
        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (snapshot) {
                    alert("loginUser.uid");
                    var childData =snapshot.val();
                    total_post[total_post.length]  =  str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Streetaddress:<br>" + childData.Streetaddress + "</p>" + str_btw + "City:<br>" + childData.City +  "</p>" + str_btw + "Zipcode:<br>" + childData.Zipcode + "</p>" + str_btw + "First_name:<br>" + childData.First_name + str_after_content;
                    first_count += 1;
                });
                document.getElementById('post_list2').innerHTML = total_post.join('');

                //add listener
                postsRef.on('users/' + loginUser.uid + '/Payment' + '/child_added', function (data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData =data.val();
                        total_post[total_post.length]  = str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Streetaddress:<br>" + childData.Streetaddress + "</p>" + str_btw + "City:<br>" + childData.City +  "</p>" + str_btw + "Zipcode:<br>" + childData.Zipcode + "</p>" + str_btw + "First_name:<br>" + childData.First_name + str_after_content;
                        document.getElementById('post_list2').innerHTML = total_post.join('');
                    }
                });
            })

        

        */

    
    }());