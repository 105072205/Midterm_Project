/* jslint browser: true*/
/*global $*/

// https://github.com/jasonmoo/t.js





(function(){function c(a){this.t=a}function l(a,b){for(var e=b.split(".");e.length;){if(!(e[0]in a))return!1;a=a[e.shift()]}return a}function d(a,b){return a.replace(h,function(e,a,i,f,c,h,k,m){var f=l(b,f),j="",g;if(!f)return"!"==i?d(c,b):k?d(m,b):"";if(!i)return d(h,b);if("@"==i){e=b._key;a=b._val;for(g in f)f.hasOwnProperty(g)&&(b._key=g,b._val=f[g],j+=d(c,b));b._key=e;b._val=a;return j}}).replace(k,function(a,c,d){return(a=l(b,d))||0===a?"%"==c?(new Option(a)).innerHTML.replace(/"/g,"&quot;"):
a:""})}var h=/\{\{(([@!]?)(.+?))\}\}(([\s\S]+?)(\{\{:\1\}\}([\s\S]+?))?)\{\{\/\1\}\}/g,k=/\{\{([=%])(.+?)\}\}/g;c.prototype.render=function(a){return d(this.t,a)};window.t=c})();
// end of 't';


////////////
(function(){

Number.prototype.to_$ = function () {
  return "$" + parseFloat( this ).toFixed(2);
};
String.prototype.strip$ = function () {
  return this.split("$")[1];
};

//var buy_which = getElementById("buy_which");

var app = {

  shipping : 5.00,
  products : [
      {
        "name" : "Oversized sweater with slogan",
        "price" : "31.78",
        "img" : "https://static.bershka.net/4/photos2/2018/V/0/2/p/2054/130/600//2054130600_1_1_1.jpg?t=1520515689987",
        "desc" : "Red temptation ."
      },
      {
        "name" : "Long printed jumpsuit",
        "price" : "66.66",
        "img" : "https://static.bershka.net/4/photos2/2018/V/0/1/p/3524/962/500/01/3524962500_1_1_3.jpg?t=1523974982845",
        "desc" : "Stunning"
      },
      {
        "name" : "Ecologically grown cotton T-shirt with print",
        "price" : "38.65",
        "img" : "https://static.bershka.net/4/photos2/2018/V/0/1/p/2178/899/675//2178899675_1_1_3.jpg?t=1524172801285",
        "desc" : "Mysterious red"
      },
      {
        "name" : "Floral print shirt",
        "price" : "24.99",
        "img" : "https://static.bershka.net/4/photos2/2018/V/0/2/p/0951/966/512//0951966512_1_1_1.jpg?t=1522949042696",
        "desc" : "Fluorescent"
      },
      {
        "name" : "Sports bermuda shorts",
        "price" : "12.99",
        "img" : "https://static.bershka.net/4/photos2/2018/V/0/2/p/3424/322/600//3424322600_1_1_1.jpg?t=1520965556106",
        "desc" : "Pure white"
      },
      {
        "name" : "Technical jacket",
        "img" : "https://static.bershka.net/4/photos2/2018/V/0/2/p/1301/609/047//1301609047_1_1_3.jpg?t=1522160092275",
        "price" : "87.87",
        "desc" : "Ravishing Yellow"
      }
    ],

  removeProduct: function () {
    "use strict";

    var item = $(this).closest(".shopping-cart--list-item");

    item.addClass("closing");
    window.setTimeout( function () {
      item.remove();
      app.updateTotals();
    }, 500); // Timeout for css animation
  },

  addProduct: function () {
    "use strict";

    var qtyCtr = $(this).prev(".product-qty"),
        quantity = parseInt(qtyCtr.html(), 10) + 1;

    app.updateProductSubtotal(this, quantity);
  },

  subtractProduct: function () {
    "use strict";

    var qtyCtr = $(this).next(".product-qty"),
        num = parseInt(qtyCtr.html(), 10) - 1,
        quantity = num <= 0 ? 0 : num;

    app.updateProductSubtotal(this, quantity);
  },

  updateProductSubtotal: function (context, quantity) {
    "use strict";

    var ctr = $(context).closest(".product-modifiers"),
        productQtyCtr = ctr.find(".product-qty"),
        productPrice = parseFloat(ctr.data("product-price")),
        subtotalCtr = ctr.find(".product-total-price"), 
////////////////////////////////////////////////////////////////////
        productname = String(ctr.data("product-name")),

        subtotalPrice = quantity * productPrice;
  
////////////////////////////////
        firebase.database().ref('Buying-items/' + firebase.auth().currentUser.uid ).push().set({
          buy: productname ,
          price: productPrice
      })
//////////////////////////////

    productQtyCtr.html(quantity);
    subtotalCtr.html( subtotalPrice.to_$() );

    app.updateTotals();
  },

  updateTotals: function () {
    "use strict";

    var products = $(".shopping-cart--list-item"),
        subtotal = 0,
        shipping;

    for (var i = 0; i < products.length; i += 1) {
      subtotal += parseFloat( $(products[i]).find(".product-total-price").html().strip$() );
    }

    shipping = (subtotal > 0 && subtotal < (100 / 1.06)) ? app.shipping : 0;

    $("#subtotalCtr").find(".cart-totals-value").html( subtotal.to_$() );
    $("#taxesCtr").find(".cart-totals-value").html( (subtotal * 0.06).to_$() );
    $("#totalCtr").find(".cart-totals-value").html( (subtotal * 1.06 + shipping).to_$() );
    $("#shippingCtr").find(".cart-totals-value").html( shipping.to_$() );
  },

  attachEvents: function () {
    "use strict";

    $(".product-remove").on("click", app.removeProduct);
    $(".product-plus").on("click", app.addProduct);
    $(".product-subtract").on("click", app.subtractProduct);
  },

  setProductImages: function () {
    "use strict";

    var images = $(".product-image"),
        ctr,
        img;

    for (var i = 0; i < images.length; i += 1) {
      ctr = $(images[i]),
      img = ctr.find(".product-image--img");

      ctr.css("background-image", "url(" + img.attr("src") + ")");
      img.remove();
    }
  },

  renderTemplates: function () {
    "use strict";

    var products = app.products,
        content = [],
        template = new t( $("#shopping-cart--list-item-template").html() );

    for (var i = 0; i < products.length; i += 1) {
      content[i] = template.render(products[i]);
    }

    $("#shopping-cart--list").html(content.join(""));
  }

};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialize Firebase
var config = {
  apiKey: "AIzaSyA7EXXjGd9w84eWujCKRvqZR9egGcNY_6Y",
  authDomain: "project-9ff11.firebaseapp.com",
  databaseURL: "https://project-9ff11.firebaseio.com",
  projectId: "project-9ff11",
  storageBucket: "project-9ff11.appspot.com",
  messagingSenderId: "456493136521"
};
firebase.initializeApp(config);

const db=firebase.database();



//get elements
const paythismuch = document.getElementById('paythismuch');
const btntopay = document.getElementById('btntopay');
const status = document.getElementById('status');
const gender = document.getElementById('gender');

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" + "</span><a class='dropdown-item' href='cart.html'>My Cart</a></span>"+ "</span><a class='dropdown-item' href='db.html'>My Profile</a></span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
              window.location.assign("index.html");
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });


    btntopay.addEventListener('click', function () { 
          var newpostref = firebase.database().ref('Paythismuch').push();
          newpostref.set({
            Email: user_email,
            Paythismuch : paythismuch.innerText
          });
  });



app.renderTemplates();
app.setProductImages();
app.attachEvents();
/////


}());


