
(function(){


    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyA7EXXjGd9w84eWujCKRvqZR9egGcNY_6Y",
        authDomain: "project-9ff11.firebaseapp.com",
        databaseURL: "https://project-9ff11.firebaseio.com",
        projectId: "project-9ff11",
        storageBucket: "project-9ff11.appspot.com",
        messagingSenderId: "456493136521"
    };
    firebase.initializeApp(config);
    
    //handle on firebase db
    const db=firebase.database();
    
    
    
    //get elements
    const message = document.getElementById('nickname');
    const phone = document.getElementById('phone');
    const gender = document.getElementById('gender');
    const age = document.getElementById('age');
    const write = document.getElementById('write');
    const read = document.getElementById('read');
    const status = document.getElementById('status');
    
    /*
    var storageRef = firebase.storage().ref();
    
    var uploadFileInput = document.getElementById("uploadFileInput");
    uploadFileInput.addEventListener("change", function(){
          var file = this.files[0];
          var uploadTask = storageRef.child('images/'+file.name).put(file);
          uploadTask.on('state_changed', function(snapshot){
            // 觀察狀態變化，例如：progress, pause, and resume
    
            // 取得檔案上傳狀態，並用數字顯示
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
    
                console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
    
                console.log('Upload is running');
                break;
            }
          }, function(error) {
            // Handle unsuccessful uploads
    
          }, function() {
            // Handle successful uploads on complete
    
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
    
            var downloadURL = uploadTask.snapshot.downloadURL;
          });
    },false);
    */
    
    
    
    
    
        var user_email = '';
        firebase.auth().onAuthStateChanged(function (user) {
            var menu = document.getElementById('dynamic-menu');
            if (user) {
                user_email = user.email;
                menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" + "</span><a class='dropdown-item' href='cart.html'>My Cart</a></span>"+ "</span><a class='dropdown-item' href='db.html'>My Profile</a></span>";
                var logout_button = document.getElementById('logout-btn');
                logout_button.addEventListener('click', function () {
                    window.location.assign("index.html");
                    firebase.auth().signOut()
                        .then(function () {
                            alert('Sign Out!')
                        })
                        .catch(function (error) {
                            alert('Sign Out Error!')
                        });
                });
            } else {
                menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
                document.getElementById('post_list').innerHTML = "";
            }
        });
    
    
        write.addEventListener('click', e=>{
    
            loginUser = firebase.auth().currentUser;
     
        
            const id = (new Date).getTime();
        
            firebase.database().ref('Profile/' + loginUser.uid ).set({
                email: loginUser.email,
                Nickname : nickname.value,
                Age : age.value,
                Phone : phone.value,
                Gender : gender.value
            })
        })
    
    /*
        //post_txt = document.getElementById('comment');
        write.addEventListener('click', function () {
            //if (post_txt.value != "") {
                loginUser = firebase.auth().currentUser;
    
                var newpostref = firebase.database().ref('com_list').push();
                newpostref.set({
                    Nickname : nickname.value,
                    Age : age.value,
                    Gender : gender.value,
                    email: user_email,
                    Phone : phone.value,
                    email2: loginUser.email
                });
                //post_txt.value = "";
            //}
        });*/
    
        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
        var str_btw =  "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
        var str_after_content = "</p></div></div>\n";
    /*
        <div class='my-3 p-3 bg-white rounded box-shadow'>
            <h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6>
                <div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>
                    <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
                        <strong class='d-block text-gray-dark'>childData.email</strong>
                        childData.age
                    </p>
                    <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
                        childData.Nickname
                    </p>
                    <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
                        childData.Gender
                    </p>
                    <p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>
                        childData.Phone
                    </p>
                </div>
        </div>*/
    
    
        /* ????????????????????????????????????????????????/  want to show all the time
        var userRef = firebase.database().ref('users');
            userRef.on('value', function(snapshot) {
                var childData =snapshot.val();
            document.getElementById('post_list').innerHTML = str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Age:<br>" + childData.Age + "</p>" + str_btw + "Nickname:<br>" + childData.Nickname +  "</p>" + str_btw + "Gender:<br>" + childData.Gender + "</p>" + str_btw + "Phone:<br>" + childData.Phone + str_after_content;
        });
    */
    
    
        write.addEventListener('click', e=>{
    
        firebase.database().ref('/Profile/' + loginUser.uid).once('value').then(function(snapshot) {
            var childData =snapshot.val();
            document.getElementById('post_list').innerHTML = str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Age:<br>" + childData.Age + "</p>" + str_btw + "Nickname:<br>" + childData.Nickname +  "</p>" + str_btw + "Gender:<br>" + childData.Gender + "</p>" + str_btw + "Phone:<br>" + childData.Phone + str_after_content;
        });
    })
    
      /* 
        var postsRef = db.ref('/users/' + loginUser.uid);
    
        //if(childData.uid == user.uid){
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (snapshot) {
                        document.getElementById('post_list').innerHTML = str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Age:<br>" + childData.Age + "</p>" + str_btw + "Nickname:<br>" + childData.Nickname +  "</p>" + str_btw + "Gender:<br>" + childData.Gender + "</p>" + str_btw + "Phone:<br>" + childData.Phone + str_after_content;
                        
                    });
    
                })
        
            .catch(e => console.log(e.message));
    
    
        var postsRef = db.ref('/users/' + loginUser.uid);
        var total_post = [];
        var first_count = 0;
        var second_count = 0;
    
        //if(childData.uid == user.uid){
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        total_post[total_post.length] = str_before_username + childData.email + "<br><br>" + "</strong>" + "</p>" + str_btw + "Age:<br>" + childData.Age + "</p>" + str_btw + "Nickname:<br>" + childData.Nickname +  "</p>" + str_btw + "Gender:<br>" + childData.Gender + "</p>" + str_btw + "Phone:<br>" + childData.Phone + str_after_content;
                        first_count += 1;
                    });
                    document.getElementById('post_list').innerHTML = total_post.join('');
    
                    //add listener
                    postsRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                        }
                    });
                })
        
            .catch(e => console.log(e.message));
       // }
    */
    
    /************************************************* *
    var userDataRef = db.ref("Nickname").orderByKey();
    userDataRef.once("nickname.value")
      .then(function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          var key = childSnapshot.key;
          var childData = childSnapshot.val();              // childData will be the actual contents of the child
    
          var name_val = childSnapshot.val().email;
          var id_val = childSnapshot.val().Nickname;
          document.getElementById("name").innerHTML = name_val;
          document.getElementById("id").innerHTML = id_val;
      });
     });
    }());*/
    
    //add signup event
    /*
    read.addEventListener('click', e => {
        status.innerHTML = '';
        const messages= db.ref('messages');
    
        messages.once('value')
            .then(function(dataSnapshot){
                var data = dataSnapshot.val();
                var keys = Object.keys(data);
    
                keys.foeEach(function(key){
                    console.log(data[key]);
                    status.innerHTML += JSON.stringify(data[key]) + '<br>';
                });
        });
    })*/
    
    }());
    
    
    
    
    /*
    //write to
    write.addEventListener('click', e=>{
        const Nickname= db.ref('Nickname');
        const Age= db.ref('Age');
        const Gender= db.ref('Gender');
        const Phone= db.ref('Phone');
    
        const id = (new Date).getTime();
    
        //write to db
        //Nickname
        Nickname.child(id).set({
                email: user_email,
                'Nickname' : nickname.value
            })
            .then(function(){
                status.innerHTML = "Wrote to DB!!";
            });
        //Age
        Age.child(id).set({
                email: user_email,
                'Age' : age.value
            })
            .then(function(){
                status.innerHTML = "Wrote to DB!!";
            });
        //Phone number
        Phone.child(id).set({
                email: user_email,
                'Phone' : phone.value
            })
            .then(function(){
                status.innerHTML = "Wrote to DB!!";
            });
        //Gender
        Gender.child(id).set({
                email: user_email,
                'Gender' : gender.value
            })
            .then(function(){
                status.innerHTML = "Wrote to DB!!";
            });
    })
    
    */